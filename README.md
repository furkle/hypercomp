# HYPERCOMP ![logo](https://furkleindustries.com/hypercomp/logo_small_nonvector.png)
## Introduction

**HYPERCOMP** is an upcoming hypertext competition for stories, novels, and games. It is intended to fill a gap in the existing ecosystem, which has numerous competitions and jams, but none specifically for hypertext.

Many writers currently enter IFComp and Spring Thing - IFComp in particular had its busiest year ever last year - so why create a whole new competition?

* Creates a context in which hypertext can be judged in its own space, on its own terms, by its own creators and consumers.
* Creates a space in which hypertext innovation becomes an overriding, existential desire.
* Brings those specifically interested in hypertext, as opposed to in parser or other non-hypertext IF, together, increasing networking and socialization.

##Installation
To install a development build of the hypercomp system, first ensure that you have Node.js and npm (which comes with Node.js) installed. Both of these can be downloaded simultaneously at <https://nodejs.org>.

Next, make sure you have git installed. You can download git from <https://git-scm.org>.

Open your console (whether bash on a UNIX or Git Bash on Windows), then type `git clone https://bitbucket.org/hypercomp.git` to clone the repository, and all the working files, into a `hypercomp` folder in the current directory.

Finally navigate to the hypercomp directory with whichever console or application with which you use npm, and type `npm start`. The development server will launch, and a tab navigated to `localhost:3000` should open in your default browser, showing the HYPERCOMP app. Any modifications made to the files contained in `src/` will cause the dev environment to automatically reload to reflect the changes.