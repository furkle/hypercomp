import React from 'react';
import ReactDOM from 'react-dom';
import ReactTestUtils from 'react-addons-test-utils';
import renderer from 'react-test-renderer';
import { Router, browserHistory, Route, IndexRoute } from 'react-router';

import App from './App.js';

import { NavBarItem } from './components/NavBar/NavBar.js';

// panes
import Home from './panes/home/home.js';
import News from './panes/news/news.js';
import Current from './panes/current/current.js';
import Previous from './panes/previous/previous.js';
import About from './panes/about/about.js';
import Enter from './panes/enter/enter.js';
import Login from './panes/login/login.js';
import Profile from './panes/profile/profile.js';

//////// global smoke tests
describe('index', () => {
    // mock localStorage
    beforeEach(() => {
        window.localStorage = {};
    });

    afterEach(() => {
        delete window.localStorage;
    });

    it('renders without crashing', () => {
        const baseUrl = process.env.PUBLIC_URL;

        ReactTestUtils.renderIntoDocument(
            <Router history={browserHistory}>
                <Route path={baseUrl + "/"} component={App}>
                    <IndexRoute component={Home} />
                    <Route path={baseUrl + "/news"} component={News} />
                    <Route path={baseUrl + "/current"} component={Current} />
                    <Route path={baseUrl + "/previous"} component={Previous} />
                    <Route path={baseUrl + "/about"} component={About} />
                    <Route path={baseUrl + "/enter"} component={Enter} />
                    <Route path={baseUrl + "/login"} component={Login} />
                    <Route path={baseUrl + "/profile"} component={Profile} />
                </Route>
            </Router>);
    });

    // not entirely sure if this is doing anything
    it('matches the snapshot', () => {
        const baseUrl = process.env.PUBLIC_URL;

        const component = renderer.create(
            <Router history={browserHistory}>
                <Route path={baseUrl + "/"} component={App}>
                    <IndexRoute component={Home} />
                    <Route path={baseUrl + "/news"} component={News} />
                    <Route path={baseUrl + "/current"} component={Current} />
                    <Route path={baseUrl + "/previous"} component={Previous} />
                    <Route path={baseUrl + "/about"} component={About} />
                    <Route path={baseUrl + "/enter"} component={Enter} />
                    <Route path={baseUrl + "/login"} component={Login} />
                    <Route path={baseUrl + "/profile"} component={Profile} />
                </Route>
            </Router>);

        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
});
////////