export function setOpenFor(openFor) {
	return {
		openFor,
		type: 'setOpenFor',
	}
}

export function setToken(token) {
	return {
		token,
		type: 'setToken',
	};
}

export function setVotes(votes) {
	return {
		votes,
		type: 'setVotes',
	}
}

export function setPanes(panes) {
	return {
		panes,
		type: 'setPanes',
	};
}

export function setSelectedPane(selectedPane) {
	return {
		selectedPane,
		type: 'setSelectedPane',
	};
}

export function setModal(modal) {
	return {
		modal,
		type: 'setModal',
	};
}

export function setCurrentYearAllEntries(currentYearAllEntries) {
	return {
		currentYearAllEntries,
		type: 'setCurrentYearAllEntries',
	}
}

export function setPreviousYearsAllEntries(previousYearsAllEntries) {
	return {
		previousYearsAllEntries,
		type: 'setPreviousYearsAllEntries',
	}
}