// redux
import { combineReducers } from 'redux';

// reducers
import {
	openForReducer,
	panesReducer,
	selectedPaneReducer,
	tokenReducer,
	profileReducer,
	votesReducer,
	currentYearAllEntriesReducer,
	previousYearsAllEntriesReducer,
	modalReducer,
	editingEntryReducer,
} from './appReducers';

import {
	usernameReducer,
	passwordReducer,
	loginErrorReducer,
	createAccountErrorReducer,
} from './panes/login/loginReducers';

import {
	profileEditingReducer,
	profileErrorReducer,
} from './panes/profile/profileReducers';

import {
	hamburgerDropdownVisibilityReducer,
} from './components/HamburgerMenu/HamburgerMenuReducers';

const indexReducer = combineReducers({
	openFor: openForReducer,
	panes: panesReducer,
	selectedPane: selectedPaneReducer,
    token: tokenReducer,
    profile: profileReducer,
    votes: votesReducer,
    currentYearAllEntries: currentYearAllEntriesReducer,
    previousYearsAllEntries: previousYearsAllEntriesReducer,
    modal: modalReducer,
    username: usernameReducer,
    password: passwordReducer,
    loginError: loginErrorReducer,
    createAccountError: createAccountErrorReducer,
    profileEditing: profileEditingReducer,
    profileError: profileErrorReducer,
    hamburgerDropdownVisibility: hamburgerDropdownVisibilityReducer,
    editingEntry: editingEntryReducer,
});

export default indexReducer;