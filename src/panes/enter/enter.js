// react
import React, { Component, } from 'react';
import { Link, } from 'react-router';

// redux
import { connect, } from 'react-redux';
import store from '../../store';

// css
import './enter.css';

class EnterPane extends Component {
	render() {
		const baseUrl = process.env.PUBLIC_URL;

		let openStatus = <span className="Enter-statusClosed">closed</span>;

		if (this.props.openFor === 'intents') {
			openStatus = (
				<span>
					<span className="Enter-statusOpen">open</span>
					<span> for intents</span>
				</span>
			);
		} else if (this.props.openFor === 'entries') {
			openStatus = (
				<span>
					<span className="Enter-statusOpen">open</span>
					<span> for entries</span>
				</span>
			);
		} else if (this.props.openFor === 'votes') {
			openStatus = (
				<span>
					<span className="Enter-statusOpen">open</span>
					<span> for judging</span>
				</span>
			);
		}

		return (
			<div className="Enter paneContainer">
				<p className="Enter-openStatus">
					HYPERCOMP is {openStatus}.
				</p>

				<p className="Enter-intentsTimespan">
					HYPERCOMP is open for intents between November 1st at midnight EST
					and November 31st at 11:59pm EST.
				</p>

				<p className="Enter-entriesTimespan">
					Entries can be submitted between December 1st at midnight and
					December 31st at 11:59am.
				</p>

				<p className="Enter-judgingTimespan">
					The competition is open for judging between January 1st and January
					31st.
				</p>

				<hr className="newSection" />

				<p className="Enter-listNote body">
					To enter HYPERCOMP, you need five things:
				</p>

				<ol className="Enter-list">
					<li>
						An email address.
					</li>

					<li>
						{"A HYPERCOMP account. You can create one "}
						<Link to={baseUrl + "/login#createAccount"}>
							here
						</Link>
						{"."}
					</li>

					<li>
						A hypertext.
					</li>

					<li>
						A title for your hypertext.
					</li>

					<li>
						A cover image (400px square) for your entry.
					</li>
				</ol>

				<hr className="newSection" />

				<p className="Enter-submitOrEditInstructions">
					{"To submit an intent or edit an entry, log in "}
					<Link to={baseUrl + "/login"}>here</Link>
					{"."}
				</p>
			</div>
		);
	}
}

function mapStateToProps() {
	const state = store.getState();

	return {
		openFor: state.openFor,
	};
}

export default connect(mapStateToProps)(EnterPane);