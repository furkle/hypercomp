// react
import React, { Component, } from 'react';

// modals
import RulesModal from '../../modals/RulesModal/RulesModal';

// css
import './about.css';

class AboutPane extends Component {
	constructor() {
		super();

		this.createRulesModal = this.createRulesModal.bind(this);
	}

	render() {
		return (
			<div className="About paneContainer">
				<p className="About-opener header">
					A competition for those who love creating and reading hypertext fiction.
				</p>

				<p className="body">
					Do you create fiction that branches outwardly, or distantly, based on which links are
					clicked by the user or where they've been before? Do you want to join others
					in a competition for groundbreaking new hypertext fiction?
				</p>

				<p className="subheader">
					Welcome to HYPERCOMP.
				</p>

				<p className="body">
					No gag rules, freedom to edit whenever and however you please, and a
					group of judges and fellow authors invested in making and discussing
					hypertext.
				</p>

				<p className="subheader">
					Bring new dynamics to your release.
				</p>

				<p className="body">
					On the author's side, HYPERCOMP emphasizes the ability to
					structure your work, and your release, as you please. Want to have
					a multi-week story arc with new content every day? You can do that.
					Want to run an ARG over the course of the comp? You can do that too.
				</p>

				<p className="subheader">
					Higher-stakes voting.
				</p>

				<p className="body">
					Instead of allocating a vote between 1 and 10 to each entry, voters must
					draw upon their reserve of 40 points. Sprinkle points around one at a time
					and spread the love. Or go all in and throw all your power, and all 40 points,
					behind your absolute, ultimate favorite. The choices are yours.
				</p>

				<button
					className="wideButton"
					onClick={this.createRulesModal}>
					<span>Rules</span>
				</button>
				
				<p className="About-attribution footer">
					A Furkle Industries production.
				</p>
			</div>
		);
	}

	componentDidMount() {
		if (location.hash === '#rules') {
			this.createRulesModal();
		}
	}

	createRulesModal() {
		if (location.hash === '#rules') {
			this.props.createModal(<RulesModal />);
		} else {
			location.hash = 'rules';
		}
	}
}

export default AboutPane;