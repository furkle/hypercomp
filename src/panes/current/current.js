// react
import React, { Component, } from 'react';

// redux
import { connect, } from 'react-redux';
import store from '../../store';

// css
import './current.css';

import EntryVoting from '../../components/EntryVoting/EntryVoting';

class CurrentPane extends Component {
	render() {
		const list = this.props.entries.map(entry => {
			return (
				<EntryVoting
					key={entry.name}
					id={entry.id}
					name={entry.name}
					license={entry.license} />
			);
		});

		return (
			<div className="Current paneContainer">
				<h1 className="Current-title header">
					This Year's Entries
				</h1>

				{list}
			</div>
		);
	}
}

function mapStateToProps() {
	const state = store.getState();

	return {
		entries: state.currentYearAllEntries,
	};
}

export default connect(mapStateToProps)(CurrentPane);