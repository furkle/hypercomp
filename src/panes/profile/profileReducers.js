export function profileNameReducer(previous = '', action) {
    if (action.type === 'setProfileName') {
        if (action.name && typeof(action.name) === 'string') {
            return action.name;
        }
    } else if (action.type === 'setProfile') {
    	if (action.profile.name && typeof(action.profile.name) === 'string') {
    		return action.profile.name;
    	}
    }

    return previous;
}

export function profileEmailReducer(previous = '', action) {
    if (action.type === 'setProfileEmail') {
        if (action.email && typeof(action.email) === 'string') {
            return action.email;
        }
    } else if (action.type === 'setProfile') {
    	if (typeof(action.profile.email) === 'string') {
    		return action.profile.email;
    	}
    }
     
    return previous;
}

export function profileTwitterReducer(previous = '', action) {
	if (action.type === 'setProfileTwitter') {
		if (action.twitter && typeof(action.twitter) === 'string') {
			return action.twitter;
		}
	} else if (action.type === 'setProfile') {
    	if (typeof(action.profile.twitter) === 'string') {
    		return action.profile.twitter;
    	}
    }

	return previous;
}

export function profileHomepageReducer(previous = '', action) {
	if (action.type === 'setProfileHomepage') {
		if (typeof(action.homepage) === 'string') {
			return action.homepage;
		}
	} else if (action.type === 'setProfile') {
    	if (typeof(action.profile.homepage) === 'string') {
    		return action.profile.homepage;
    	}
    }

	return previous;
}

export function profileCurrentYearEntriesReducer(previous = [], action) {
    if (action.type === 'setProfileCurrentYearEntries') {
        if (action.currentYearEntries &&
        	typeof(action.currentYearEntries) === 'object' &&
        	'length' in action.currentYearEntries)
        {
            return action.currentYearEntries;
        }
    } else if (action.type === 'setProfile') {
    	if (action.profile.currentYearEntries &&
    		typeof(action.profile.currentYearEntries) === 'object' &&
    		'length' in action.profile.currentYearEntries)
    	{
    		return action.profile.currentYearEntries;
    	}
    }
     
    return previous;
}

export function profileEditingReducer(previous = false, action) {
	if (action.type === 'setProfileEditing') {
		if (typeof(action.profileEditing) === 'boolean') {
			return action.profileEditing;
		}
	}

	return previous;
}

export function profileErrorReducer(previous = '', action) {
    if (action.type === 'setProfileError') {
        if (typeof(action.profileError) === 'string') {
            return action.profileError;
        }
    }

    return previous;
}