export function setProfile(profile) {
	return {
		profile,
		type: 'setProfile',
	};
}

export function setProfileName(name) {
	return {
		name,
		type: 'setProfileName',
	};
}

export function setProfileEmail(email) {
	return {
		email,
		type: 'setProfileEmail',
	};
}

export function setProfileTwitter(twitter) {
	return {
		twitter,
		type: 'setProfileTwitter',
	};
}

export function setProfileHomepage(homepage) {
	return {
		homepage,
		type: 'setProfileHomepage',
	};
}

export function setProfileCurrentYearEntries(currentYearEntries) {
	return {
		currentYearEntries,
		type: 'setProfileCurrentYearEntries',
	};
}

export function setProfileEditing(profileEditing) {
	return {
		profileEditing,
		type: 'setProfileEditing',
	};
}