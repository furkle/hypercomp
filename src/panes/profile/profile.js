// react
import React, { Component, } from 'react';
import { browserHistory, } from 'react-router';

// redux
import { connect, } from 'react-redux';
import store from '../../store';
import { setSelectedPane, } from '../../appActions';

import {
	setProfileName,
	setProfileEmail,
	setProfileTwitter,
	setProfileHomepage,
	setProfileEditing,
	setProfileError,
} from './profileActions';

// css
import './profile.css';

// components
import EntryEditing from '../../components/EntryEditing/EntryEditing';

const baseUrl = process.env.PUBLIC_URL;

class ProfilePane extends Component {
	render() {
		const opts = {};

		if (!this.props.editing) {
			 opts.readOnly = 'readOnly'; 
		}

		const list = (this.props.profile.currentYearEntries || [])
			.map(entry => {
				const linkTo = this.props.openForJudging ?
					'https://compentries.furkleindustries.com/' + entry.id :
					'';

				return <EntryEditing
					key={entry.name}
					name={entry.name}
					id={entry.id}
					license={entry.license}
					linkTo={linkTo}
					openFor={this.props.openFor}
					createModal={this.props.createModal} />;
			});

		const MAX_INTENTS = 3;

		return (
			<div className="Profile paneContainer">
				<section id="profileInfo">
					<h2 className="Profile-infoHeader subheader">Profile Information</h2>
					<div className="Profile-infoContainer">
						<div className="Profile-infoPairContainer">
							<label
								className="Profile-nameLabel Profile-label"
								htmlFor="Profile-name">
								Name:
							</label>

							<input
								id="Profile-name"
								className={"Profile-name Profile-input body" + (this.props.editing ? "" :  " Profile-readonly")}
								value={this.props.profile.name}
								onChange={e => this.setName(e.target.value)}
								{...opts} />
						</div>

						<div className="Profile-infoPairContainer">
							<label
								className="Profile-emailLabel Profile-label"
								htmlFor="Profile-email">
								Email:
							</label>
							
							<input
								id="Profile-email"
								className={"Profile-email Profile-input body" + (this.props.editing ? "" :  " Profile-readonly")}
								value={this.props.profile.email}
								onChange={e => this.setEmail(e.target.value)}
								{...opts} />
						</div>

						<div className="Profile-infoPairContainer">
							<label
								className="Profile-homepageLabel Profile-label"
								htmlFor="Profile-homepage">
								Homepage:
							</label>
							
							<input
								id="Profile-homepage"
								className={"Profile-homepage Profile-input body" + (this.props.editing ? "" :  " Profile-readonly")}
								value={this.props.profile.homepage}
								onChange={e => this.setTwitter(e.target.value)}
								{...opts} />
						</div>

						<div className="Profile-infoPairContainer">
							<label
								className="Profile-twitterLabel Profile-label"
								htmlFor="Profile-twitter">
								Twitter:
							</label>
							
							<input
								id="Profile-twitter"
								className={"Profile-homepage Profile-input body" + (this.props.editing ? "" :  " Profile-readonly")}
								value={this.props.profile.twitter}
								onChange={e => this.setHomepage(e.target.value)}
								{...opts} />
						</div>
					</div>

					<p className="Profile-error">{this.props.error}</p>

					<button
						className={"Profile-editAccount wideButton" + (this.props.editing ? " Profile-invisible" : "")}
						onClick={() => this.setProfileEditing(true)}>
						<span>
							Edit Profile Details
						</span>
					</button>

					<div className={"Profile-doubleButtonContainer wideButton" + (this.props.editing ? "" : " Profile-invisible")}>
						<button className="Profile-confirmEditAccount wideButton">
							<span>Confirm</span>
						</button>

						<button
							className="Profile-cancelEditAccount wideButton"
							onClick={() => this.setProfileEditing(false)}>
							<span>Cancel</span>
						</button>
					</div>
				</section>

				<hr className="newSection" />

				{
					this.props.openFor !== 'none' ?
						<section className="Profile-currentYearEntries body">
							<h2 className="Profile-entriesHeader subheader">
								My Current Entries
							</h2>

							{list.length ? list : "No entries."}
							
							{this.props.openFor === 'intents' &&
								list.length < MAX_INTENTS ?
								<button className="Profile-newIntent wideButton">
									New Intent
								</button> :
								null}
						</section> :
						<span>HYPERCOMP is closed.</span>
				}

				<hr className="newSection" />

				<button
					className="Profile-logout wideButton"
					onClick={this.props.appLogout}>
					<span>
						Log Out
					</span>
				</button>
			</div>
		);
	}

	componentDidMount() {
		if (!localStorage.token) {
			store.dispatch(setSelectedPane('login'));
			browserHistory.push(baseUrl + '/login');
		}
	}

	setProfileEditing(value) {
		store.dispatch(setProfileEditing(value));
	}

	setName(value) {
		store.dispatch(setProfileName(value));
	}

	setEmail(value) {
		store.dispatch(setProfileEmail(value));
	}

	setHomepage(value) {
		store.dispatch(setProfileHomepage(value));
	}

	setTwitter(value) {
		store.dispatch(setProfileTwitter(value));
	}

	setProfileError(value) {
		store.dispatch(setProfileError(value));
	}
}

function mapStateToProps() {
	const state = store.getState();

	return {
		openFor: state.openFor,
		profile: state.profile,
		editing: state.profileEditing,
		error: state.profileError,
	};
}

export default connect(mapStateToProps)(ProfilePane);