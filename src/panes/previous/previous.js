// react
import React, { Component } from 'react';

// redux
import { connect, } from 'react-redux';
import store from '../../store';

// css
import './previous.css';

// components
import PreviousYear from '../../components/PreviousYear/PreviousYear.js';

class PreviousPane extends Component {
	render() {
		const list = Object.keys(this.props.previousYearsAllEntries).map(year => {
			return (
				<PreviousYear
					key={Number(year)}
					year={Number(year)}
					entries={this.props.previousYearsAllEntries[year]} />
			);
		});

		return (
			<div className="Previous paneContainer">
				<p className="body">
					Anything below is test data. No hypercomps have been held yet.
				</p>

				{list}
			</div>
		);
	}
}

function mapStateToProps() {
	const state = store.getState();

	return {
		previousYearsAllEntries: state.previousYearsAllEntries,
	};
}

export default connect(mapStateToProps)(PreviousPane);