// react
import React, { Component, } from 'react';
import { Link, } from 'react-router';

import './home.css';

// images
import logo from '../../images/logo.svg';

class HomePane extends Component {
	render() {
		const baseUrl = process.env.PUBLIC_URL;

		return (
			<div className="Home paneContainer">
				<div className="Home-headerBanner">
					<div className="Home-firstColumn Home-column">
						<p className="header">
							A competition for hypertext stories, novels, and games.
						</p>
					</div>

					<div className="Home-secondColumn Home-column">
						<img className="Home-logo" src={logo} alt="logo" />
					</div>
				</div>

				<p className="Home-subheader subheader">
					Opening sometime soon.
				</p>

				<p className="body">
					{"Click "}
					<Link to={baseUrl + "/about"}>
						About
					</Link>
					{" to learn more."}

				</p>

				<p className="body">
					{"Click "}
					<Link to={baseUrl + "/login#createAccount"}>
						Create Account
					</Link>
					{" to get involved."}
				</p>
			</div>
		);
	}
}

export default HomePane;