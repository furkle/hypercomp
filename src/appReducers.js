// redux
import { combineReducers, } from 'redux';

import {
    profileNameReducer,
    profileEmailReducer,
    profileTwitterReducer,
    profileHomepageReducer,
    profileCurrentYearEntriesReducer,
} from './panes/profile/profileReducers';

import {
    editingEntryComponentReducer,
    editingEntryIdReducer,
    editingEntryNameReducer,
    editingEntryImageReducer,
    editingEntryFileReducer,
    editingEntryLicenseReducer,
    editingEntryErrorReducer,
} from './modals/EditEntryModal/EditEntryModalReducers';

// metadata for each pane
import panesSource from './panesSource';

export function openForReducer(previous = 'none', action) {
    if (action.type === 'setOpenFor') {
        if (action.openFor && typeof(action.openFor) === 'string') {
            return action.openFor;
        }
    }

    return previous;
}

export function votesReducer(previous = {}, action) {
    if (action.type === 'setVotes') {
        if (action.votes && typeof(action.votes) === 'object') {
            return action.votes;
        }
    }

    return previous;
}

export function panesReducer(previous = panesSource, action) {
    if (action.type === 'setPanes') {
        if (action.panes && typeof(action.panes) === 'object') {
            return action.panes;
        }
    }

    return previous;
}

const startPane =
    location.pathname.slice(location.pathname.lastIndexOf('/') + 1) ||
    'home';

export function selectedPaneReducer(previous = startPane, action) {
    if (action.type === 'setSelectedPane') {
        if (action.selectedPane && typeof(action.selectedPane) === 'string') {
            return action.selectedPane;
        }
    }

    return previous;
}

export function tokenReducer(previous = null, action) {
    if (action.type === 'setToken') {
        if (action.token !== undefined) {
            return action.token;
        }
    }

    return previous;
}

const profileReducer = combineReducers({
    name: profileNameReducer,
    email: profileEmailReducer,
    twitter: profileTwitterReducer,
    homepage: profileHomepageReducer,
    currentYearEntries: profileCurrentYearEntriesReducer,
});

export { profileReducer };

export function modalReducer(previous = null, action) {
    if (action.type === 'setModal') {
        if (typeof(action.modal) === 'object') {
            return action.modal;
        }
    }

    return previous;
}

const editingEntryReducer = combineReducers({
    component: editingEntryComponentReducer,
    id: editingEntryIdReducer,
    name: editingEntryNameReducer,
    image: editingEntryImageReducer,
    file: editingEntryFileReducer,
    license: editingEntryLicenseReducer,
    error: editingEntryErrorReducer,
});

export { editingEntryReducer };

export function currentYearAllEntriesReducer(previous = [], action) {
    if (action.type === 'setCurrentYearAllEntries') {
        if (typeof(action.currentYearAllEntries) === 'object' &&
            'length' in action.currentYearAllEntries)
        {
            return action.currentYearAllEntries;
        }
    }

    return previous;
}

export function previousYearsAllEntriesReducer(previous = [], action) {
    if (action.type === 'setPreviousYearsAllEntries') {
        if (typeof(action.previousYearsAllEntries) === 'object' &&
            'length' in action.previousYearsAllEntries)
        {
            return action.previousYearsAllEntries;
        }
    }

    return previous;
}