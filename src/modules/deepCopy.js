export default function deepCopy(object) {
	if (typeof(object) !== 'object') {
		throw new Error('Input argument was not of type object, or was null.');
	}

	return JSON.parse(JSON.stringify(object));
}