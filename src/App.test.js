import React from 'react';
import ReactDOM from 'react-dom';
import ReactTestUtils from 'react-addons-test-utils';
import { Router, browserHistory, Route, IndexRoute } from 'react-router';

import App from './App';

import { NavBarItem } from './components/NavBar/NavBar.js';

// panes
import Home from './panes/home/home.js';
import News from './panes/news/news.js';
import Current from './panes/current/current.js';
import Previous from './panes/previous/previous.js';
import About from './panes/about/about.js';
import Enter from './panes/enter/enter.js';
import Login from './panes/login/login.js';
import Profile from './panes/profile/profile.js';

//////// global smoke tests
describe('App', () => {
	// mock localStorage
	beforeEach(() => {
		window.localStorage = {};
	});

	afterEach(() => {
		delete window.localStorage;
	});


	it('renders without crashing', () => {
		const div = document.createElement('div');
		ReactDOM.render(<App />, div);
	});

	it('starts at the home pane', () => {
		const component = ReactTestUtils.renderIntoDocument(<App />);
		expect(component.state.selectedPane).toEqual('home');
	});

	it('has 8 NavBarItems', () => {
		const component = ReactTestUtils.renderIntoDocument(<App />);
		const navBarItems = ReactTestUtils.scryRenderedComponentsWithType(
		    component,
		    NavBarItem);

		expect(navBarItems.length).toEqual(8);
	});

	it('switches to the home pane on correct NavBarItem click', () => {
		const baseUrl = process.env.PUBLIC_URL;

		const component = ReactTestUtils.renderIntoDocument(
			<Router history={browserHistory}>
		        <Route path={baseUrl + "/"} component={App}>
		            <IndexRoute component={Home} />
		            <Route path={baseUrl + "/news"} component={News} />
		            <Route path={baseUrl + "/current"} component={Current} />
		            <Route path={baseUrl + "/previous"} component={Previous} />
		            <Route path={baseUrl + "/about"} component={About} />
		            <Route path={baseUrl + "/enter"} component={Enter} />
		            <Route path={baseUrl + "/login"} component={Login} />
		            <Route path={baseUrl + "/profile"} component={Profile} />
		        </Route>
		    </Router>);
		const navBarItems = ReactTestUtils.scryRenderedDOMComponentsWithClass(
			component,
			'NavBarItem');

		navBarItems.forEach(navBarItem => {
			if (navBarItem.textContent === 'Home') {
				ReactTestUtils.Simulate.click(navBarItem);
			}
		});

		const app = ReactTestUtils.findRenderedComponentWithType(component, App);

		expect(app.state.selectedPane).toEqual('home');
	});

	it('switches to the news pane on correct NavBarItem click', () => {
		const baseUrl = process.env.PUBLIC_URL;

		const component = ReactTestUtils.renderIntoDocument(
			<Router history={browserHistory}>
		        <Route path={baseUrl + "/"} component={App}>
		            <IndexRoute component={Home} />
		            <Route path={baseUrl + "/news"} component={News} />
		            <Route path={baseUrl + "/current"} component={Current} />
		            <Route path={baseUrl + "/previous"} component={Previous} />
		            <Route path={baseUrl + "/about"} component={About} />
		            <Route path={baseUrl + "/enter"} component={Enter} />
		            <Route path={baseUrl + "/login"} component={Login} />
		            <Route path={baseUrl + "/profile"} component={Profile} />
		        </Route>
		    </Router>);
		const navBarItems = ReactTestUtils.scryRenderedDOMComponentsWithClass(
			component,
			'NavBarItem');

		navBarItems.forEach(navBarItem => {
			if (navBarItem.textContent === 'News') {
				ReactTestUtils.Simulate.click(navBarItem);
			}
		});

		const app = ReactTestUtils.findRenderedComponentWithType(component, App);

		expect(app.state.selectedPane).toEqual('news');
	});

	it('switches to the current year pane on correct NavBarItem click', () => {
		const baseUrl = process.env.PUBLIC_URL;

		const component = ReactTestUtils.renderIntoDocument(
			<Router history={browserHistory}>
		        <Route path={baseUrl + "/"} component={App}>
		            <IndexRoute component={Home} />
		            <Route path={baseUrl + "/news"} component={News} />
		            <Route path={baseUrl + "/current"} component={Current} />
		            <Route path={baseUrl + "/previous"} component={Previous} />
		            <Route path={baseUrl + "/about"} component={About} />
		            <Route path={baseUrl + "/enter"} component={Enter} />
		            <Route path={baseUrl + "/login"} component={Login} />
		            <Route path={baseUrl + "/profile"} component={Profile} />
		        </Route>
		    </Router>);
		const navBarItems = ReactTestUtils.scryRenderedDOMComponentsWithClass(
			component,
			'NavBarItem');

		navBarItems.forEach(navBarItem => {
			if (navBarItem.textContent === 'This Year') {
				ReactTestUtils.Simulate.click(navBarItem);
			}
		});

		const app = ReactTestUtils.findRenderedComponentWithType(component, App);

		expect(app.state.selectedPane).toEqual('current');
	});

	it('switches to the past years pane on correct NavBarItem click', () => {
		const baseUrl = process.env.PUBLIC_URL;

		const component = ReactTestUtils.renderIntoDocument(
			<Router history={browserHistory}>
		        <Route path={baseUrl + "/"} component={App}>
		            <IndexRoute component={Home} />
		            <Route path={baseUrl + "/news"} component={News} />
		            <Route path={baseUrl + "/current"} component={Current} />
		            <Route path={baseUrl + "/previous"} component={Previous} />
		            <Route path={baseUrl + "/about"} component={About} />
		            <Route path={baseUrl + "/enter"} component={Enter} />
		            <Route path={baseUrl + "/login"} component={Login} />
		            <Route path={baseUrl + "/profile"} component={Profile} />
		        </Route>
		    </Router>);
		const navBarItems = ReactTestUtils.scryRenderedDOMComponentsWithClass(
			component,
			'NavBarItem');

		navBarItems.forEach(navBarItem => {
			if (navBarItem.textContent === 'Past Years') {
				ReactTestUtils.Simulate.click(navBarItem);
			}
		});

		const app = ReactTestUtils.findRenderedComponentWithType(component, App);

		expect(app.state.selectedPane).toEqual('previous');
	});

	it('switches to the about pane on correct NavBarItem click', () => {
		const baseUrl = process.env.PUBLIC_URL;

		const component = ReactTestUtils.renderIntoDocument(
			<Router history={browserHistory}>
		        <Route path={baseUrl + "/"} component={App}>
		            <IndexRoute component={Home} />
		            <Route path={baseUrl + "/news"} component={News} />
		            <Route path={baseUrl + "/current"} component={Current} />
		            <Route path={baseUrl + "/previous"} component={Previous} />
		            <Route path={baseUrl + "/about"} component={About} />
		            <Route path={baseUrl + "/enter"} component={Enter} />
		            <Route path={baseUrl + "/login"} component={Login} />
		            <Route path={baseUrl + "/profile"} component={Profile} />
		        </Route>
		    </Router>);
		const navBarItems = ReactTestUtils.scryRenderedDOMComponentsWithClass(
			component,
			'NavBarItem');

		navBarItems.forEach(navBarItem => {
			if (navBarItem.textContent === 'About') {
				ReactTestUtils.Simulate.click(navBarItem);
			}
		});

		const app = ReactTestUtils.findRenderedComponentWithType(component, App);

		expect(app.state.selectedPane).toEqual('about');
	});

	it('switches to the enter pane on correct NavBarItem click', () => {
		const baseUrl = process.env.PUBLIC_URL;

		const component = ReactTestUtils.renderIntoDocument(
			<Router history={browserHistory}>
		        <Route path={baseUrl + "/"} component={App}>
		            <IndexRoute component={Home} />
		            <Route path={baseUrl + "/news"} component={News} />
		            <Route path={baseUrl + "/current"} component={Current} />
		            <Route path={baseUrl + "/previous"} component={Previous} />
		            <Route path={baseUrl + "/about"} component={About} />
		            <Route path={baseUrl + "/enter"} component={Enter} />
		            <Route path={baseUrl + "/login"} component={Login} />
		            <Route path={baseUrl + "/profile"} component={Profile} />
		        </Route>
		    </Router>);
		const navBarItems = ReactTestUtils.scryRenderedDOMComponentsWithClass(
			component,
			'NavBarItem');

		navBarItems.forEach(navBarItem => {
			if (navBarItem.textContent === 'Enter') {
				ReactTestUtils.Simulate.click(navBarItem);
			}
		});

		const app = ReactTestUtils.findRenderedComponentWithType(component, App);

		expect(app.state.selectedPane).toEqual('enter');
	});

	it('switches to the login pane on correct NavBarItem click', () => {
		const baseUrl = process.env.PUBLIC_URL;

		const component = ReactTestUtils.renderIntoDocument(
			<Router history={browserHistory}>
		        <Route path={baseUrl + "/"} component={App}>
		            <IndexRoute component={Home} />
		            <Route path={baseUrl + "/news"} component={News} />
		            <Route path={baseUrl + "/current"} component={Current} />
		            <Route path={baseUrl + "/previous"} component={Previous} />
		            <Route path={baseUrl + "/about"} component={About} />
		            <Route path={baseUrl + "/enter"} component={Enter} />
		            <Route path={baseUrl + "/login"} component={Login} />
		            <Route path={baseUrl + "/profile"} component={Profile} />
		        </Route>
		    </Router>);
		const navBarItems = ReactTestUtils.scryRenderedDOMComponentsWithClass(
			component,
			'NavBarItem');

		navBarItems.forEach(navBarItem => {
			if (navBarItem.textContent === 'Login') {
				ReactTestUtils.Simulate.click(navBarItem);
			}
		});

		const app = ReactTestUtils.findRenderedComponentWithType(component, App);

		expect(app.state.selectedPane).toEqual('login');
	});

	it('switches to the profile pane on correct NavBarItem click', () => {
		const baseUrl = process.env.PUBLIC_URL;

		const component = ReactTestUtils.renderIntoDocument(
			<Router history={browserHistory}>
		        <Route path={baseUrl + "/"} component={App}>
		            <IndexRoute component={Home} />
		            <Route path={baseUrl + "/news"} component={News} />
		            <Route path={baseUrl + "/current"} component={Current} />
		            <Route path={baseUrl + "/previous"} component={Previous} />
		            <Route path={baseUrl + "/about"} component={About} />
		            <Route path={baseUrl + "/enter"} component={Enter} />
		            <Route path={baseUrl + "/login"} component={Login} />
		            <Route path={baseUrl + "/profile"} component={Profile} />
		        </Route>
		    </Router>);
		const navBarItems = ReactTestUtils.scryRenderedDOMComponentsWithClass(
			component,
			'NavBarItem');

		navBarItems.forEach(navBarItem => {
			if (navBarItem.textContent === 'Profile') {
				ReactTestUtils.Simulate.click(navBarItem);
			}
		});

		const app = ReactTestUtils.findRenderedComponentWithType(component, App);

		expect(app.state.selectedPane).toEqual('profile');
	});
});
////////
