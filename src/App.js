// react
import React, { Component, } from 'react';
import { browserHistory, } from 'react-router';

// redux
import { connect, } from 'react-redux';
import store from './store';
window.store = store;

import {
	setOpenFor,
	setPanes,
	setSelectedPane,
	setToken,
	setVotes,
	setModal,
	setCurrentYearAllEntries,
	setPreviousYearsAllEntries,
} from './appActions';

import { setLoginError, } from './panes/login/loginActions';

import { setProfile, } from './panes/profile/profileActions';

import {
	setHamburgerDropdownVisibility,
} from './components/HamburgerMenu/HamburgerMenuActions';

// css
import './App.css';

// modules
import makeRequest from './modules/makeRequest';
import deepCopy from './modules/deepCopy';

// components
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Modal from './components/Modal/Modal';

// modals
import RulesModal from './modals/RulesModal/RulesModal';
import CreateAccountModal from './modals/CreateAccountModal/CreateAccountModal';

const baseUrl = process.env.PUBLIC_URL;

class App extends Component {
	constructor() {
		super();

		this.appLogin = this.appLogin.bind(this);
		this.renderLogin = this.renderLogin.bind(this);
		this.getProfile = this.getProfile.bind(this);
		this.getVotes = this.getVotes.bind(this);
		this.appLogout = this.appLogout.bind(this);
		this.navBarItemClick = this.navBarItemClick.bind(this);
		this.createModal = this.createModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
	}

	render() {
		const id = this.props.selectedPane;
	
		let child = React.cloneElement(this.props.children, {
			appLogin: this.appLogin,
			appLogout: this.appLogout,
			closeModal: this.closeModal,
			createModal: this.createModal,
		});

		const backgroundStyle = (this.props.panes[id] || {}).backgroundStyle;

		return (
      		<div className="App">
	        	<div className="App-backgroundColor"></div>
	        	<div className="App-backgroundImage" style={backgroundStyle}></div>

	        	<Header navBarItemClick={this.navBarItemClick} />

	        	{child}

				<Footer />

	        	<div className={"modalContainer" + (this.props.modal ? "" : " hidden")}>
	        		{this.props.modal}
	        		<div
	        			className="modalBackground"
	        			onClick={this.closeModal}>
	        		</div>
	        	</div>
      		</div>
    	);
	}

	componentDidMount() {
		this.getOpenStatus();

		const token = localStorage.token;
		if (!store.getState().token) {
			store.dispatch(setToken(token));
		}

		if (token) {
			this.renderLogin(JSON.parse(token));
		}

		this.getCurrentYearAllEntries().catch(xhr => {
			return Promise.reject();
		}).then(xhr => {
			const responseObj = JSON.parse(xhr.responseText);
			store.dispatch(setCurrentYearAllEntries(
				responseObj.currentYearAllEntries));
		});

		this.getPreviousYearsAllEntries().catch(xhr => {
			return Promise.reject();
		}).then(xhr => {
			const responseObj = JSON.parse(xhr.responseText);
			store.dispatch(setPreviousYearsAllEntries(
				responseObj.previousYearsAllEntries));
		});

		document.body.onkeydown = e => {
			if (e.keyCode === 27) {
				this.closeModal();
			}
		};

		window.onhashchange = () => {
			if (this.props.selectedPane === 'about' &&
				location.hash === '#rules')
			{
				this.createModal(<RulesModal />);
			} else if (this.props.selectedPane === 'login' &&
				location.hash === '#createAccount')
			{
				this.createModal(<CreateAccountModal />);
			} else if (this.props.selectedPane === 'profile' &&
				location.hash.startsWith('#editEntry-'))
			{
				const dashLoc = location.hash.indexOf('-');
				if (dashLoc !== -1) {
					const id = location.hash.slice(dashLoc + 1);

					// get the Entry component, not the wrapping Connect
					const entry = this.props.editingComponent._self;
					if (id === entry.props.id) {
						entry.editEntry();
					}
				}
			} else if (!location.hash || location.hash === '#') {
				store.dispatch(setModal(null));
			}
		};

		if (!location.hash || location.hash === '#') {
			store.dispatch(setModal(null));
		}

		// TODO: find a way to remove this interval
		setInterval(() => {
			if ((!location.hash || location.hash === '#') &&
				store.getState().modal)
			{
				store.dispatch(setModal(null));
			}
		}, 500);
	}

	getOpenStatus() {
		makeRequest({
			method: 'GET',

			url: 'https://furkleindustries.com/hypercomp/getOpenStatus.php',
		}).catch(xhr => {
			console.log('Open status endpoint cannot be reached. Sorry for ' +
				'the inconvenience; please contact the webmaster with one ' +
				'of the icons in the footer. ' + xhr.responseText);
			Promise.reject();
		}).then(xhr => {
			try {
				const responseObj = JSON.parse(xhr.responseText);

				store.dispatch(setOpenFor(responseObj.openFor));
			} catch (e) {
				throw new Error('Open status JSON object could not be deserialized.');
			}
		});
	}

	appLogin() {
		makeRequest({
			method: 'POST',
			
			url: 'https://furkleindustries.com/hypercomp/login.php',
			
			params: {
				username: this.props.username,
				password: this.props.password,
			},

			headers: {
				'Content-type': 'application/x-www-form-urlencoded',
			},
		}).catch(xhr => {
			try {
				const unknown = 'Unknown error.';
				const responseObj = JSON.parse(xhr.responseText);
				store.dispatch(setLoginError(responseObj.error || unknown));

				setTimeout(() => {
					const loginError = this.props.loginError;
					if (loginError === responseObj.error ||
						loginError === unknown)
					{
						store.dispatch(setLoginError(''));
					}
				}, 6000);
			} catch (err) {
				const unknown = 'Unknown error. Could not parse response ' +
					'object.';
				store.dispatch(setLoginError(unknown));

				setTimeout(() => {
					if (this.props.loginError === unknown) {
						store.dispatch(setLoginError(''));
					}
				}, 6000);
			}

			// don't allow execution to continue
			return Promise.reject();
		}).then(xhr => {
			const responseObj = JSON.parse(xhr.responseText);

			const token = responseObj.token;
			store.dispatch(setToken(token));
			localStorage.token = JSON.stringify(token);

			const profileCopy = deepCopy(this.props.profile);
			profileCopy.currentYearEntries = responseObj.currentYearEntries;
			store.dispatch(setProfile(profileCopy));

			this.renderLogin(token, true);
		});
	}

	renderLogin(token, gotoProfile) {
		this.getProfile(token).catch(xhr => {
			// delete the cached, now invalid token
			delete localStorage.token;

			if (this.props.selectedPane === 'profile') {
				// redirect to the login page
				store.dispatch(setSelectedPane('login'));
				browserHistory.push(baseUrl + '/login');
			}

			// don't allow execution to continue
			return Promise.reject();
		}).then(xhr => {
			try {
				const responseObj = JSON.parse(xhr.responseText);
				store.dispatch(setProfile(responseObj.profile));

				const panesCopy = deepCopy(this.props.panes);
				panesCopy.login.visible = false;
				panesCopy.profile.visible = true;
				store.dispatch(setPanes(panesCopy));

				// redirect to the profile page
				if (gotoProfile) {
					store.dispatch(setSelectedPane('profile'));
					browserHistory.push(baseUrl + '/profile');
				}
			} catch (e) {
				console.log('Couldn\'t deserialize profile. ' + e.message);
				return Promise.reject();
			}
		});

		this.getVotes(token).catch(xhr => {
			console.log('Couldn\'t get votes.');
			return Promise.reject();
		}).then(xhr => {
			try {
				const responseObj = JSON.parse(xhr.responseText);
				store.dispatch(setVotes(responseObj.votes));
			} catch (e) {
				console.log('Couldn\'t deserialize votes. ' + e.message);
			}
		});
	}

	getProfile(token) {
		return makeRequest({
			method: 'GET',
			url: 'https://furkleindustries.com/hypercomp/getProfile.php?' +
				'token=' + encodeURIComponent(JSON.stringify(token)),
		});
	}

	getVotes(token) {
		return makeRequest({
			method: 'GET',
			url: 'https://furkleindustries.com/hypercomp/getVotes.php?' +
				'token=' + encodeURIComponent(JSON.stringify(token)),
		});
	}

	getPreviousYearsAllEntries() {
		return makeRequest({
			method: 'GET',
			url: 'https://furkleindustries.com/hypercomp/' +
				'getPreviousYearsAllEntries.php',
		});
	}

	getCurrentYearAllEntries() {
		return makeRequest({
			method: 'GET',
			url: 'https://furkleindustries.com/hypercomp/' +
				'getCurrentYearAllEntries.php?',
		});
	}

	appLogout() {
		const panesCopy = deepCopy(this.props.panes);
		panesCopy.login.visible = true;
		panesCopy.profile.visible = false;

		store.dispatch(setPanes(panesCopy));

		store.dispatch(setToken(null));

		store.dispatch(setProfile({
			name: '',
			email: '',
			twitter: '',
			homepage: '',
			currentYearEntries: [],
			editing: false,
		}));

		store.dispatch(setToken(null));

		delete localStorage.token;

		// redirect to the login page
		store.dispatch(setSelectedPane('login'));
		browserHistory.push(baseUrl + '/login');
	}

	navBarItemClick(id) {
		store.dispatch(setSelectedPane(id));
		store.dispatch(setHamburgerDropdownVisibility(false));
	}

	createModal(content) {
		const modal = <Modal content={content} closeModal={this.closeModal} />;
		store.dispatch(setModal(modal));
	}

	closeModal(content) {
		store.dispatch(setModal(null));

		location.hash = '';

		delete document.body.onkeydown;
	}
}

function mapStateToProps() {
	const state = store.getState();

	return {
		panes: state.panes,
		selectedPane: state.selectedPane,
		token: state.token,
		profile: state.profile,
		username: state.username,
		password: state.password,
		modal: state.modal,
		editingComponent: state.editingEntry.component,
	};
}

export default connect(mapStateToProps)(App);