// react
import React from 'react';
import ReactDOM from 'react-dom';

// react-router
import { Router, Route, IndexRoute, browserHistory, } from 'react-router';

// redux
import { Provider, } from 'react-redux';
import store from './store';

// app
import App from './App';

// panes
import Home from './panes/home/home';
import News from './panes/news/news';
import Current from './panes/current/current';
import Previous from './panes/previous/previous';
import About from './panes/about/about';
import Enter from './panes/enter/enter';
import Login from './panes/login/login';
import Profile from './panes/profile/profile';

const baseUrl = process.env.PUBLIC_URL;

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path={baseUrl + "/"} component={App}>
                <IndexRoute component={Home} />
                <Route path={baseUrl + "/news"} component={News} />
                <Route path={baseUrl + "/current"} component={Current} />
                <Route path={baseUrl + "/previous"} component={Previous} />
                <Route path={baseUrl + "/about"} component={About} />
                <Route path={baseUrl + "/enter"} component={Enter} />
                <Route path={baseUrl + "/login"} component={Login} />
                <Route path={baseUrl + "/profile"} component={Profile} />
            </Route>
        </Router>
    </Provider>,
    document.querySelector('#root')
);