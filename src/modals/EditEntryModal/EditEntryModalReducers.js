export function editingEntryComponentReducer(previous = null, action) {
    if (action.type === 'setEditingEntryComponent') {
        if (action.component && typeof(action.component) === 'object') {
            return action.component;
        }
    }

    return previous;
}

export function editingEntryIdReducer(previous = null, action) {
    if (action.type === 'setEditingEntryId') {
        if (action.id > 0) {
            return action.id;
        }
    }

    return previous;
}

export function editingEntryNameReducer(previous = '', action) {
    if (action.type === 'setEditingEntryName') {
        if (typeof(action.name) === 'string') {
            return action.name;
        }
    }

    return previous;
}

export function editingEntryImageReducer(previous = null, action) {
    if (action.type === 'setEditingEntryImage') {
        if (action.image === null || action.image instanceof File) {
            return action.image;
        }
    }
     
    return previous;
}

export function editingEntryFileReducer(previous = null, action) {
    if (action.type === 'setEditingEntryFile') {
        if (action.file === null || action.file instanceof File) {
            return action.file;
        }
    }

    return previous;
}

export function editingEntryLicenseReducer(previous = '', action) {
    if (action.type === 'setEditingEntryLicense') {
        if (typeof(action.license) === 'string') {
            return action.license;
        }
    }

    return previous;
}

export function editingEntryErrorReducer(previous = '', action) {
    if (action.type === 'setEditingEntryError') {
        if (typeof(action.error) === 'string') {
            return action.error;
        }
    }

    return previous;
}