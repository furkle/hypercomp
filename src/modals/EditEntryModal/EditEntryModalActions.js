export function setEditingEntryComponent(component) {
	return {
		component,
		type: 'setEditingEntryComponent',
	}
}

export function setEditingEntryId(id) {
	return {
		id,
		type: 'setEditingEntryId',
	}
}

export function setEditingEntryName(name) {
	return {
		name,
		type: 'setEditingEntryName',
	};
}

export function setEditingEntryImage(image) {
	return {
		image,
		type: 'setEditingEntryImage',
	};
}

export function setEditingEntryFile(file) {
	return {
		file,
		type: 'setEditingEntryFile',
	};
}

export function setEditingEntryLicense(license) {
	return {
		license,
		type: 'setEditingEntryLicense',
	};
}

export function setEditingEntryError(error) {
	return {
		error,
		type: 'setEditingEntryError',
	};
}