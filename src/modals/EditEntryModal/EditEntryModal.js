// react
import React, { Component, } from 'react';

// redux
import { connect, } from 'react-redux';
import store from '../../store';
import {
	setEditingEntryName,
	setEditingEntryImage,
	setEditingEntryFile,
	setEditingEntryLicense,
	setEditingEntryError,
} from './EditEntryModalActions';

import {
	setProfileCurrentYearEntries,
} from '../../panes/profile/profileActions';

// jquery
import $ from 'jquery';

// css
import './EditEntryModal.css';

// modules
import deepCopy from '../../modules/deepCopy';

class EditEntryModal extends Component {
	constructor() {
		super();

		this.doEditEntry = this.doEditEntry.bind(this);
	}

	render() {
		const nameOpts = {};
		if (this.props.openFor !== 'intents') {
			nameOpts.readOnly = 'readonly';
		}

		const licenseOpts = {};
		if (this.props.openFor !== 'entries') {
			licenseOpts.readOnly = 'readonly';
		}

		return (
			<div className="EditEntryModal">
				<h1 className="header">
					{"Edit "}
					{
						this.props.openFor === "intents" ?
							"Intent" :
							"Entry"
					}
				</h1>
				<section className="EditEntry-editContainer">
					<div className="EditEntry-infoPairContainer">
						<label
							className="EditEntry-nameLabel EditEntry-label body"
							htmlFor="EditEntry-name">
							Name:
						</label>
						<input
							className={"EditEntry-input body" +
								(this.props.openFor === "intents" ?
									"" :
									" EditEntry-readonly")}
							id="EditEntry-name"
							value={this.props.name}
							onChange={this.onNameChange}
							onKeyDown={e => {
								if (e.keyCode === 13) {
									this.doEditEntry();
								}
							}}
							{...nameOpts} />
					</div>

					{
						this.props.openFor === 'entries' ||
							this.props.openFor === 'votes' ?
								<div className="EditEntry-infoPairContainer">
									<label
										className="EditEntry-imageLabel EditEntry-label body"
										htmlFor="EditEntry-image">
										Entry Image:
									</label>
									<input
										type="file"
										className="EditEntry-input body"
										id="EditEntry-image"
										onChange={this.onImageChange} />
								</div> :
								null
					}

					{
						this.props.openFor === 'entries' ||
							this.props.openFor === 'votes' ?
							<div className="EditEntry-infoPairContainer">
								<label
									className="EditEntry-fileLabel EditEntry-label body"
									htmlFor="EditEntry-file">
									Entry File:
								</label>
								<input
									type="file"
									className="EditEntry-input body"
									id="EditEntry-file"
									onChange={this.onFileChange} />
							</div> :
							null
					}

					<div className="EditEntry-infoPairContainer">
						<label
							className="EditEntry-licenseLabel EditEntry-label body"
							htmlFor="EditEntry-license">
							License:
						</label>
						<input
							className={"EditEntry-input body" +
								(this.props.openFor === "intents" ||
									this.props.openFor === "entries" ?
									"" :
									" EditEntry-readonly")}
							id="EditEntry-license"
							value={this.props.license}
							onChange={this.onLicenseChange}
							onKeyDown={e => {
								if (e.keyCode === 13) {
									this.doEditEntry();
								}
							}}
							{...licenseOpts} />
					</div>
				</section>

				<button
					className="EditEntry-button wideButton centerHorizontallyRelative"
					onClick={this.doEditEntry}>
					<span>Confirm Changes</span>
				</button>

				<p className="EditEntry-error">
					{this.props.error}
				</p>
			</div>
		);
	}

	componentDidMount() {
		// blank the file entries
		store.dispatch(setEditingEntryImage(null));
		store.dispatch(setEditingEntryFile(null));
	}

	onNameChange(e) {
		store.dispatch(setEditingEntryName(e.target.value));
	}

	onImageChange(e) {
		store.dispatch(setEditingEntryImage(e.target.files[0]));
	}

	onFileChange(e) {
		store.dispatch(setEditingEntryFile(e.target.files[0]));
	}

	onLicenseChange(e) {
		store.dispatch(setEditingEntryLicense(e.target.value));
	}

	doEditEntry() {
		const DISPLAY_ERROR_FOR = 6000;

		const formData = new FormData();

		formData.append('token', this.props.token);
		formData.append('id', this.props.id);
		formData.append('name', this.props.name);
		formData.append('imageFile', this.props.image);
		formData.append('entryFile', this.props.file);
		formData.append('license', this.props.license);

		$.ajax({
			url: 'https://furkleindustries.com/hypercomp/updateEntry.php',
			data: formData,
			processData: false,
			contentType: false,
			type: 'POST',
			complete: xhr => {
				let responseObj;

				try {
					responseObj = JSON.parse(xhr.responseText);	
				} catch (err) {
					const deserializeFailure = 'There was a failure ' +
						'deserializing the server response. Please contact the ' +
						'webmaster.';
					store.dispatch(setEditingEntryError(deserializeFailure));

					setTimeout(() => {
						if (this.props.error === deserializeFailure) {
							store.dispatch(setEditingEntryError(''));
						}
					}, DISPLAY_ERROR_FOR);

					return;
				}

				if (responseObj.error) {
					const error = responseObj.error || 'Unknown error.';
					store.dispatch(setEditingEntryError(error));

					setTimeout(() => {
						if (this.props.error === error) {
							store.dispatch(setEditingEntryError(''));
						}
					}, DISPLAY_ERROR_FOR);

					return;
				}

				const success = 'Update successful.';
				store.dispatch(setEditingEntryError(success));

				const currentYearEntries = deepCopy(
					store.getState().profile.currentYearEntries);
				const cyeModified = currentYearEntries.map(entry => {
					// save changes to editingEntry
					if (entry.id === this.props.id) {
						entry.name = this.props.name;
						entry.license = this.props.license;
					}

					return entry;
				});

				store.dispatch(setProfileCurrentYearEntries(cyeModified));

				setTimeout(() => {
					if (this.props.error === success) {
						this.props.closeModal();
						store.dispatch(setEditingEntryError(''));
					}
				}, DISPLAY_ERROR_FOR);
			}
		});
	}
}

function mapStateToProps() {
	const state = store.getState();
	return {
		token: state.token,
		openFor: state.openFor,
		name: state.editingEntry.name,
		id: state.editingEntry.id,
		image: state.editingEntry.image,
		file: state.editingEntry.file,
		license: state.editingEntry.license,
		error: state.editingEntry.error,
	};
}

export default connect(mapStateToProps)(EditEntryModal);