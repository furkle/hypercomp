// react
import React, { Component, } from 'react';

// css
import './EntryCompleted.css';

class EntryCompleted extends Component {
	render() {
		return (
			<div className="EntryCompleted">
				<em className="EntryCompleted-title">{this.props.name}</em>

				{this.props.linkTo ?
					<a href={this.props.linkTo}>Link</a> :
					null}
			</div>
		);
	}
}

export default EntryCompleted;