// react
import React, { Component, } from 'react';

// redux
import store from '../../store';
import {
	setEditingEntryComponent,
	setEditingEntryId,
	setEditingEntryName,
	setEditingEntryLicense,
} from '../../modals/EditEntryModal/EditEntryModalActions';

// css
import './EntryEditing.css';

// modals
import EditEntryModal from '../../modals/EditEntryModal/EditEntryModal';

class EntryEditing extends Component {
	constructor() {
		super();

		this.editEntry = this.editEntry.bind(this);
	}

	render() {
		return (
			<div className="EntryEditing">
				<em className="EntryEditing-title">{this.props.name}</em>
				
				<button
					className="EntryEditing-edit EntryEditing-button"
					onClick={this.editEntry}>
					Edit
				</button>

				{this.props.linkTo ?
					<a href={this.props.linkTo}>Link</a> :
					null}
			</div>
		);
	}

	componentDidMount() {
		if (location.hash.startsWith('#editEntry-')) {
			const dashLoc = location.hash.indexOf('-');
			if (dashLoc !== -1) {
				const id = location.hash.slice(dashLoc + 1);
				if (id === this.props.id) {
					this.editEntry();
				}
			}
		}
	}

	editEntry() {
		const component = <EditEntryModal />;

		store.dispatch(setEditingEntryComponent(component));
		store.dispatch(setEditingEntryId(this.props.id));
		store.dispatch(setEditingEntryName(this.props.name));
		store.dispatch(setEditingEntryLicense(this.props.license));

		location.hash = 'editEntry-' + this.props.id;

		this.props.createModal(component);
	}
}

export default EntryEditing;