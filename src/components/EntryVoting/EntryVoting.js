// react
import React, { Component, } from 'react';

// redux
import { connect, } from 'react-redux';
import store from '../../store';

// css
import './EntryVoting.css';

class EntryVoting extends Component {
	constructor() {
		super();

		this.state = {
			active: false,
		};

		this.toggleVote = this.toggleVote.bind(this);
	}

	render() {
		const options = [
			<option key="null"></option>,
			<option key="1">1</option>,
			<option key="2">2</option>,
			<option key="3">3</option>,
			<option key="4">4</option>,
			<option key="5">5</option>,
			<option key="6">6</option>,
			<option key="7">7</option>,
			<option key="8">8</option>,
			<option key="9">9</option>,
			<option key="10">10</option>,
		];

		return (
			<div className="EntryVoting">
				<img
					className="EntryVoting-image"
					alt={this.props.name}
					src={"https://compentries.furkleindustries.com/" + this.props.id + "/image.png"} />

				<em className="EntryVoting-title subheader">{this.props.name}</em>
				
				{this.props.token && this.props.openFor === "votes" ?
					<button
						className={"EntryVoting-vote EntryVoting-button" + (this.state.active ? " active" : "")}
						onClick={this.toggleVote}>
						Vote
					</button> :
					null}

				{this.props.linkTo ?
					<a href={this.props.linkTo}>Link</a> :
					null}

				<div className={"EntryVoting-voteContainer" + (this.state.active ? "" : " hidden")}>
					<div className="EntryVoting-voteCategory EntryVoting-categoryOne">
						<span className="EntryVoting-categoryTitle">
							Category One
						</span>

						<select className="EntryVoting-voteSelect">
							{options}
						</select>
					</div>

					<div className="EntryVoting-voteCategory EntryVoting-categoryTwo">
						<span className="EntryVoting-categoryTitle">
							Category Two
						</span>

						<select className="EntryVoting-voteSelect">
							{options}
						</select>
					</div>

					<div className="EntryVoting-voteCategory EntryVoting-categoryThree">
						<span className="EntryVoting-categoryTitle">
							Category Three
						</span>

						<select className="EntryVoting-voteSelect">
							{options}
						</select>
					</div>

					<div className="EntryVoting-voteCategory EntryVoting-categoryFour">
						<span className="EntryVoting-categoryTitle">
							Category Four
						</span>

						<select className="EntryVoting-voteSelect">
							{options}
						</select>
					</div>

					<div className="EntryVoting-voteCategory EntryVoting-categoryFive">
						<span className="EntryVoting-categoryTitle">
							Category Five
						</span>

						<select className="EntryVoting-voteSelect">
							{options}
						</select>
					</div>
				</div>
			</div>
		);
	}

	toggleVote() {
		this.setState({
			active: !this.state.active,
		});
	}
}

function mapStateToProps() {
	const state = store.getState();

	return {
		token: state.token,
		openFor: state.openFor,
	};
}

export default connect(mapStateToProps)(EntryVoting);