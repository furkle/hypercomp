export function setHamburgerDropdownVisibility(hamburgerDropdownVisibility) {
	return {
		hamburgerDropdownVisibility,
		type: 'setHamburgerDropdownVisibility',
	}
}