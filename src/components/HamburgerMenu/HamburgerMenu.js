// react
import React, { Component, } from 'react';
import { Link, } from 'react-router';

// redux
import { connect, } from 'react-redux';
import store from '../../store';
import { setHamburgerDropdownVisibility, } from './HamburgerMenuActions';

// css
import './HamburgerMenu.css';

// images
/* https://www.iconfinder.com/icons/134216/hamburger_lines_menu_icon#size=256
 * https://creativecommons.org/licenses/by-sa/3.0/ */
import hamburgerIcon from '../../images/hamburger_icon.svg';

class HamburgerMenuClass extends Component {
	constructor() {
		super();

		this.toggleHamburgerDropdown = this.toggleHamburgerDropdown.bind(this);
	}

	render() {
		const hamburgerPanes = {};
		Object.keys(this.props.panes).forEach(pane => {
			if (this.props.panes[pane].inHamburgerMenu) {
				hamburgerPanes[pane] = this.props.panes[pane];
			}
		});

		const list = [];
		Object.getOwnPropertyNames(hamburgerPanes).forEach(name => {
			const active = this.props.selectedPane === name;
			list.push(
				<HamburgerMenuItem id={name}
					key={name}
					title={this.props.panes[name].title}
					active={active ? "true" : ""}
					visible={this.props.panes[name].visible}
					navBarItemClick={this.props.navBarItemClick} />);
		});

		return (
			<div className="HamburgerMenu">
				<div
					className="HamburgerMenu-icon"
					onClick={this.toggleHamburgerDropdown}>
					<img src={hamburgerIcon} alt="hamburger menu icon" />
				</div>

				<div className={"HamburgerMenu-dropDown" +
					(this.props.dropdownVisible ? " visible" : "")}>
					{list}
				</div>
			</div>
		);
	}

	toggleHamburgerDropdown() {
		store.dispatch(setHamburgerDropdownVisibility(!this.props.dropdownVisible));
	}
}

function mapStateToProps() {
	const state = store.getState();

	return {
		panes: state.panes,
		selectedPane: state.selectedPane,
		dropdownVisible: state.hamburgerDropdownVisibility,
	};
}

const HamburgerMenu = connect(mapStateToProps)(HamburgerMenuClass);

class HamburgerMenuItem extends Component {
	render() {
		const baseUrl = process.env.PUBLIC_URL;
		
		let to = this.props.id;
		if (to === 'home') {
			to = '';
		}

		return (
			<Link
				to={baseUrl + '/' + to}
				id={this.props.id}
				className={"HamburgerMenu-item" +
					(this.props.active ? " active" : "") +
					(this.props.visible ? "" : " hidden")}
				onClick={e => this.props.navBarItemClick(e.target.id)}>
				{this.props.title}
			</Link>
		);
	}
}

export { HamburgerMenu, HamburgerMenuItem, };	