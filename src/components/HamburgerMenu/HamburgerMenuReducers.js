export function hamburgerDropdownVisibilityReducer(previous = false, action) {
	if (action.type === 'setHamburgerDropdownVisibility') {
		if (typeof(action.hamburgerDropdownVisibility) === 'boolean') {
			return action.hamburgerDropdownVisibility;
		}
	}

	return previous;
}