// react
import React, { Component, } from 'react';

// css
import './Modal.css';

class Modal extends Component {
	render() {
		if (!this.props || !this.props.content) {
			throw new Error('No content property provided to Modal.');
		}

		const content = React.cloneElement(this.props.content, {
			closeModal: this.props.closeModal,
		});

		return (
			<div className="Modal centerHorizontallyAndVerticallyAbsolute">
				<button
					className="Modal-close"
					onClick={this.props.closeModal}>
					✖
				</button>

				<div
					className="Modal-contentContainer"
					onKeyDown={e => {
						if (e.keyCode === 27 || e === 'manual') {
							this.props.closeModal();
						}
					}}>
					{content}
				</div>
			</div>
		);
	}
}

export default Modal;