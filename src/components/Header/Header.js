// react
import React, { Component, } from 'react';

// css
import './Header.css';

// components
import { NavBar, } from '../NavBar/NavBar';
import { HamburgerMenu, } from '../HamburgerMenu/HamburgerMenu'

// images
import logo from '../../images/logo.svg';

class Header extends Component {
	render() {
		const date = new Date();
		const year = date.getFullYear();
		const month = date.getMonth();
		const day = date.getDay();
		let compYear = year;
		if (month === 1 || (month === 2 && day < 15)) {
			compYear++;
		}

		const hypercompSplit = ('HYPERCOMP ' + String(compYear))
			.split('')
			.map((aa, bb) => <span key={bb}>{aa}</span>);
		
		return (
			<div className="Header">
				<span className="Header-firstThird">
					<h1 className="Header-title">
						{hypercompSplit}
					</h1>
				</span>

				<span className="Header-secondThird">
					<img
						className="Header-icon centerHorizontallyAndVerticallyAbsolute"
						src={logo}
						alt="hypercomp icon" />
				</span>

				<span className="Header-thirdThird">
					<NavBar navBarItemClick={this.props.navBarItemClick} />

					<HamburgerMenu navBarItemClick={this.props.navBarItemClick} />
				</span>

				<div className="Header-noise"></div>
			</div>
		);
	}
}

export default Header;