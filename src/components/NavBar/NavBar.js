// react
import React, { Component, } from 'react';
import { Link, } from 'react-router';

// redux
import { connect, } from 'react-redux';
import store from '../../store';

// css
import './NavBar.css';

class NavBarClass extends Component {
	render() {
		const list = [];
		Object.getOwnPropertyNames(this.props.panes).forEach(name => {
			const active = this.props.selectedPane === name;
			list.push(
				<NavBarItem id={name}
					key={name}
					title={this.props.panes[name].title}
					active={active ? "true" : ""}
					visible={this.props.panes[name].visible}
					navBarItemClick={this.props.navBarItemClick} />);
		});

		return (
			<div className="NavBar">
				{list}
			</div>
		);
	}
}

function mapStateToProps() {
	const state = store.getState();
	return {
		panes: state.panes,
		selectedPane: state.selectedPane,
	};
}

const NavBar = connect(mapStateToProps)(NavBarClass);

class NavBarItem extends Component {
	render() {
		const baseUrl = process.env.PUBLIC_URL;
		
		let to = this.props.id;
		if (to === 'home') {
			to = '';
		}

		return (
			<Link
				to={baseUrl + '/' + to}
				id={this.props.id}
				className={"NavBarItem" +
					(this.props.active ? " active" : "") +
					(this.props.visible ? "" : " hidden")}
				onClick={e => { this.props.navBarItemClick(e.target.id); }}>
				{this.props.title}
			</Link>
		);
	}
}

export { NavBar, NavBarItem, };