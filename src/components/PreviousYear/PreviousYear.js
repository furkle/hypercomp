import React, { Component } from 'react';
import './PreviousYear.css';

import EntryCompleted from '../EntryCompleted/EntryCompleted.js';

class PreviousYear extends Component {
	render() {
		const entries = this.props.entries;
		const list = entries.map(entry => <EntryCompleted />);

		return (
			<div className="PreviousYear">
				<div className="header">{this.props.year}</div>
				{list}
			</div>
		);
	}
}

export default PreviousYear;